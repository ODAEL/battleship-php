<?php

use Battleship\Color;
use PHPUnit\Framework\TestCase;

final class AppE2ETests extends TestCase
{
    public function testPlayGameWin()
    {
        $input = "HESOYAM\nBAGUVIX\ny\nHESOYAM\nBAGUVIX\ny\nHESOYAM\nBAGUVIX\ny\nHESOYAM\nBAGUVIX\n";
        ob_start();
        try {
            system("echo \"$input\" | composer run game 2>&1");
        } catch (Exception $exception) {
        }
        $out = ob_get_contents();
        ob_end_clean();
        $this->assertStringContainsString("Welcome to Battleship", $out);
        $this->assertStringContainsString('<GAME START>', $out);
        $this->assertStringContainsString('<Round #1>', $out);
        $this->assertStringContainsString('You\'ve won the battle! But the war is not over! You\'re little sissy and I\'ll crush you next time!', $out);
        $this->assertStringContainsString('Arrrgh! You did this again! But this doesn\'t mean a shit. You will be begging for mercy in the next battle! I prepared something for you. Also, ur mom stinks!', $out);
        $this->assertStringContainsString('FUUUUU*K!!111 Now you really pissed me off! Try to do this again and see what happens, you punk!', $out);
        $this->assertStringContainsString('Okay,Okay, you got me, sir!(( Please stop beating me! I\'ll do ur laundry.', $out);
    }

    public function testPlayGameShotHits()
    {
        $input = "a1\na2\na3\na4\na5\nb1\nb2\nb3\nb4\nc1\nc2\nc3\nd1\nd2\nd3\ne1\ne2\ng8\n";
        ob_start();
        try {
            system("echo \"$input\" | composer run game 2>&1");
        } catch (Exception $exception) {
        }
        $out = ob_get_contents();
        ob_end_clean();
        $this->assertStringContainsString("Welcome to Battleship", $out);
        $this->assertStringContainsString(Color::RED."Yeah! Nice hit!", $out);
        $this->assertStringContainsString('<GAME START>', $out);
        $this->assertStringContainsString('<Round #1>', $out);
        $this->assertStringContainsString('<Round #2>', $out);
    }


    public function testPlayGameShotMisses()
    {
        $input = "a1\na2\na3\na4\na5\nb1\nb2\nb3\nb4\nc1\nc2\nc3\nd1\nd2\nd3\ne1\ne2\ne4\n";

        ob_start();
        try {
            system("echo \"$input\" | composer run game 2>&1");
        } catch (Exception $exception) {
            echo $exception->getMessage();
        }
        $out = ob_get_contents();
        ob_end_clean();
        $this->assertStringContainsString("Welcome to Battleship", $out);
        $this->assertStringContainsString("Miss", $out);
    }

}
