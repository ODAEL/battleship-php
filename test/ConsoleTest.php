<?php
declare(strict_types=1);

use Battleship\Color;

class ConsoleTest extends \PHPUnit\Framework\TestCase
{
    public function testResetForegroundColor()
    {
        $console = new Console();
        ob_start();
        $console -> resetForegroundColor();
        $out = ob_get_contents();
        ob_end_clean();
        $this ->assertEquals(Color::DEFAULT_GREY, $out);
    }

    public function testPrint()
    {
        $console = new Console();
        ob_start();
        $console->print('Test test test', Color::GREEN);
        $out = ob_get_contents();
        ob_end_clean();
        $this ->assertEquals(Color::GREEN.'Test test test', $out);
    }

    public function testPrintln()
    {
        $console = new Console();
        ob_start();
        $console->println('Test test test', Color::GREEN);
        $out = ob_get_contents();
        ob_end_clean();
        $this ->assertEquals(Color::GREEN."Test test test\n", $out);
    }
}
