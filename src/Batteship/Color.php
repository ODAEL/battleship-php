<?php

namespace Battleship;

class Color
{
    public const DEFAULT_GREY = "\e[39m";
    public const MAGENTA      = "\e[95m";
    public const CADET_BLUE   = "\e[96m";
    public const CYAN         = "\e[34m";
    public const RED          = "\e[91m";
    public const YELLOW       = "\e[93m";
    public const ORANGE       = "\e[33m";
    public const GREEN        = "\e[32m";
}