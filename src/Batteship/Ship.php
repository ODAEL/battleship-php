<?php

namespace Battleship;

class Ship
{
    /** @var string */
    private $name;

    /** @var int */
    private $size;

    /** @var string */
    private $color;

    /** @var array */
    private $positions = array();

    public function __construct(string $name, int $size, string $color = null)
    {
        $this->name = $name;
        $this->size = $size;
        $this->color = $color;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSize(): int
    {
        return $this->size;
    }

    public function getColor(): string
    {
        return $this->color;
    }

    public function addPosition($input): void
    {
        $letter = substr($input, 0, 1);
        $number = substr($input, 1, 1);

        $this->positions[] = new Position($letter, $number);
    }

    public function &getPositions(): array
    {
        return $this->positions;
    }

    public function setSize($size): void
    {
        $this->size = $size;
    }

}