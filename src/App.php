<?php

use Battleship\GameController;
use Battleship\Position;
use Battleship\Letter;
use Battleship\Color;

class App
{
    private static $myFleet = array();
    private static $enemyFleet = array();
    /** @var Console */
    private static $console;

    private static $winCount = 0;


    private static $template1 = [
        'Aircraft Carrier' => [['B', 6], ['C', 6],['D', 6],['E', 6], ['F', 6]],
        'Battleship' => [['D', 4], ['E', 4],['F', 4],['G', 4]],
        'Submarine' => [['B', 2], ['C', 2],['D', 2]],
        'Destroyer' => [['F', 2], ['G', 2],['H', 2]],
        'Patrol Boat' => [['F', 8], ['G', 8]],
    ];

    private static $template2 = [
        'Aircraft Carrier' => [['D', 8], ['E', 8],['F', 8],['G', 8], ['H', 8]],
        'Battleship' => [['A', 1], ['A', 2],['A', 3],['A', 4]],
        'Submarine' => [['E', 3], ['F', 3],['G', 3]],
        'Destroyer' => [['F', 1], ['G', 1],['H', 1]],
        'Patrol Boat' => [['D', 5], ['E', 5]],
    ];

    private static $templates = [];

    static function run(): void
    {
        self::$templates = [self::$template1, self::$template2];
        self::$console = new Console();
        self::$console->printShip();
        self::InitializeGame();
        self::StartGame();
    }

    public static function applyTemplate($fleet, $template): void
    {
        foreach ($fleet as &$ship) {
            $coords = $template[$ship->getName()];

            foreach ($coords as &$coord) {
                $ship->getPositions()[] = new Position($coord[0], $coord[1]);
            }
        }
    }

    public static function InitializeEnemyFleet(): void
    {
        self::$enemyFleet = GameController::initializeShips();
        $randomNumber = random_int(1, count(self::$templates));

        self::applyTemplate(self::$enemyFleet, self::$templates[$randomNumber - 1]);
    }

    public static function getRandomPosition(): Position
    {
        $rows = 8;
        $lines = 8;

        $letter = Letter::value(random_int(0, $lines - 1));
        $number = random_int(0, $rows - 1);

        return new Position($letter, $number);
    }

    public static function InitializeMyFleet(): void
    {
        self::$myFleet = GameController::initializeShips();

        self::$console->println("Please position your fleet (Game board has size from A to H and 1 to 8) :");

        foreach (self::$myFleet as $ship) {

            self::$console->println();
            self::$console->println(printf('Please enter the positions for the %s (size: %s)', $ship->getName(), $ship->getSize()));

            for ($i = 1; $i <= $ship->getSize(); $i++)
            {
                self::$console->print(sprintf("\nEnter position %s of %s (i.e A3):", $i, $ship->getSize()), Color::YELLOW);
                self::$console->setForegroundColor(Color::YELLOW);
                $input = readline("");
                if ($input === 'HESOYAM') {
                    self::initMyFieldStatic();
                    return;
                }
                $ship->addPosition($input);
            }
        }
    }

    public static function initMyFieldStatic(): void
    {
        self::$myFleet = GameController::initializeShips();

        self::applyTemplate(self::$myFleet, self::$template1);
    }

    public static function beep(): void
    {
        echo "\007";
    }

    public static function InitializeGame(): void
    {
        self::InitializeMyFleet();
        self::InitializeEnemyFleet();
    }

    public static function printHitOrMissMessage($isHit): void
    {
        if ($isHit) {
            self::$console->println('Yeah! Nice hit!', Color::RED);
        } else {
            self::$console->println('Miss', Color::CADET_BLUE);
        }
    }

    public static function StartGame(): void
    {
        self::$console->println("\033[2J\033[;H");
        self::$console->println("                  __");
        self::$console->println("                 /  \\");
        self::$console->println("           .-.  |    |");
        self::$console->println("   *    _.-'  \\  \\__/");
        self::$console->println("    \\.-'       \\");
        self::$console->println("   /          _/");
        self::$console->println("  |      _  /\" \"");
        self::$console->println("  |     /_\'");
        self::$console->println("   \\    \\_/");
        self::$console->println("    \" \"\" \"\" \"\" \"");

        $roundNumber = 1;



        while (true) {
            self::$console->println();
            if ($roundNumber === 1) {
                self::$console->println("----------<GAME START>----------"); // 44
            }
            self::$console->println("-----------<Round #$roundNumber>-----------");
            self::$console->println();
            self::$console->print('Enter coordinates for your shot:', Color::YELLOW);
            self::$console->setForegroundColor(Color::YELLOW);
            $position = readline("");

            if ($position === 'BAGUVIX') {
                self::processWin();
                return;
            }

            $isHit = GameController::checkIsHit(self::$enemyFleet, self::parsePosition($position));
            if ($isHit) {
                self::beep();
                self::$console->printNuke();
            }

            self::printHitOrMissMessage($isHit);
            self::$console->println();

            $position = self::getRandomPosition();
            $isHit = GameController::checkIsHit(self::$myFleet, $position);
            self::$console->println();
            self::$console->println(sprintf("Computer shoot: %s%s", $position->getColumn(), $position->getRow()));
            self::printHitOrMissMessage($isHit);

            if ($isHit) {
                self::beep();

                self::$console->println("                \\         .  ./");
                self::$console->println("              \\      .:\" \";'.:..\" \"   /");
                self::$console->println("                  (M^^.^~~:.'\" \").");
                self::$console->println("            -   (/  .    . . \\ \\)  -");
                self::$console->println("               ((| :. ~ ^  :. .|))");
                self::$console->println("            -   (\\- |  \\ /  |  /)  -");
                self::$console->println("                 -\\  \\     /  /-");
                self::$console->println("                   \\  \\   /  /");

            }

//            exit();
            $roundNumber++;
        }
    }

    public static function processWin(): void
    {
        self::$winCount++;

        switch (self::$winCount) {
            case 1:
                self::$console->printShreder(Color::RED);
                self::$console->printTextInBorder('You\'ve won the battle! But the war is not over! You\'re little sissy and I\'ll crush you next time!', Color::RED);
                break;
            case 2:
                self::$console->printShreder(Color::RED);
                self::$console->printTextInBorder('Arrrgh! You did this again! But this doesn\'t mean a shit. You will be begging for mercy in the next battle! I prepared something for you. Also, ur mom stinks!', Color::RED);
                break;
            case 3:
                self::$console->printShreder(Color::RED);
                self::$console->printTextInBorder('FUUUUU*K!!111 Now you really pissed me off! Try to do this again and see what happens, you punk!', Color::RED);
                break;
            case 4:
                self::$console->printShrederFailed(Color::MAGENTA);
                self::$console->printTextInBorder('Okay,Okay, you got me, sir!(( Please stop beating me! I\'ll do ur laundry.', Color::MAGENTA);
                exit();
                break;
        }

        while (self::askForNewGame() === false) {

        }

        self::run();
    }

    public static function askForNewGame(): bool
    {
        self::$console->print('Do you want to continue? (Y/N)', Color::YELLOW);
        $choise = readline("");

        if ($choise === 'N' || $choise === 'n') {
            self::$console->println('Bye bye!');
            exit();
        }

        if ($choise === 'Y' || $choise === 'y') {
            return true;
        }

        self::$console->println('Sorry, don\'t understand you');


        return false;
    }

    public static function parsePosition($input): Position
    {
        $letter = substr($input, 0, 1);
        $number = substr($input, 1, 1);

        if(!is_numeric($number)) {
            throw new Exception("Not a number: $number");
        }

        return new Position($letter, $number);
    }
}